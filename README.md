**Title:**
HuffingtonPost Trending Web-app

**Description:**
Single Page Web Application to display trending HuffingtonPost articles under specific section based on Twitter Trends

**Tools Required:**
1. Minimum Java version 7 (JDK & JRE)
2. Apache Maven (4.0)
3. Bitbucket.org account (project will be shared there)
4. git (1.9.3 or latest)
5. Terminal/Cygwin/Command prompt for running the maven commands
6. Apache Tomcat server (7.0)
7. MongoDb (2.6.4 or latest)

**Guide to download and install the application on Tomcat server (UNIX/LINUX/MAC):**

* Register for Twitter API and add the appropriate keys in the "Huffwebproj / src / main / resources / config.properties" file
* Configuring Apache Tomcat Server: 
* * Navigate to "tomcat-users.xml" file under "conf" directory of Apache Tomcat root
* * Edit the file to append the below xml tag under  "tomcat-users" tag of the file
```
#!xml

<role rolename="manager-script"/>
<user username="admin" password="s3cret" roles="manager-script"/>
```

* To run the server, navigate to apache tomcat server's bin directory in Terminal and execute "./startup.sh" command.
* Configuring Apache Maven: 
* * Navigate to "settings.xml" that can be found under "conf" directory of Apache Maven root folder
* * Edit the file to append the below xml tag under "servers"

```
#!xml
<server>
      <id>localhost</id>
      <username>admin</username>
      <password>s3cret</password>
</server>
```
* Clone the repository to your local machine
* Navigate to the project root folder in Terminal
* Run the below maven commands for appropriate action
* * Deploy to Tomcat server first time - "mvn clean package tomcat7:deploy"
* * Deploy to Tomcat server later on - "mvn clean package tomcat7:redeploy"
* * Deploy to Tomcat server without tests - "mvn clean -DskipTests=true package tomcat7:redeploy"
* Once the deploy/redeploy is successful. The application can be accessed at [http://localhost:8080/huffwebproj/](http://localhost:8080/huffwebproj/#)

**Application Screenshot for Results:**

* Single section select (Run time: 11:16.44 AM EST 01/07) 
![Screen Shot 2015-01-07 at 11.16.44 AM.png](https://bitbucket.org/repo/Gy97jK/images/3837287663-Screen%20Shot%202015-01-07%20at%2011.16.44%20AM.png)
* Multi section select (Run time: 11:16.20 AM EST 01/07) 
![Screen Shot 2015-01-07 at 11.16.20 AM.png](https://bitbucket.org/repo/Gy97jK/images/1235684460-Screen%20Shot%202015-01-07%20at%2011.16.20%20AM.png)
* Twitter API limit exceeded: Error is thrown to try after some time (Runtime: 11.22.39 AM EST 01/07)
![Screen Shot 2015-01-07 at 11.22.39 AM.png](https://bitbucket.org/repo/Gy97jK/images/3738547968-Screen%20Shot%202015-01-07%20at%2011.22.39%20AM.png)
* No Trending articles available (Run time: 11:51.17 AM EST 01/07)
![Screen Shot 2015-01-07 at 11.51.17 AM.png](https://bitbucket.org/repo/Gy97jK/images/467079160-Screen%20Shot%202015-01-07%20at%2011.51.17%20AM.png)