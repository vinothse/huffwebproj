<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="images/favicon.ico">
<title>HuffingtonPost Trending Articles</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/starter-template.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">HuffTrendingArticles</a>
			</div>
		</div>
	</nav>

	<div class="container">

		<div class="starter-template">
			<h1>HuffingtonPost Trending Articles</h1>
			<p class="lead">
				Please select the sections to find trending articles<br>
			</p>

			<form id="form_id" action="input">
				<select id="select_id" name="section" size="10" multiple="multiple">
					<option value="front_page">FRONT_PAGE</option>
					<option value="home">HOME</option>
					<option value="entertainment">ENTERTAINMENT</option>
					<option value="politics">POLITICS</option>
					<option value="business">BUSINESS</option>
					<option value="sports">SPORTS</option>
					<option value="worldpost">WORLDPOST</option>
					<option value="education">EDUCATION</option>
					<option value="tech">TECH</option>
					<option value="books">BOOKS</option>
					<option value="media">MEDIA</option>
					<option value="green">GREEN</option>
					<option value="comedy">COMEDY</option>
					<option value="healthy_living">HEALTHY_LIVING</option>
					<option value="arts">ARTS</option>
					<option value="science">SCIENCE</option>
					<option value="small_biz">SMALL_BIZ</option>
					<option value="crime">CRIME</option>
					<option value="weird_news">WEIRD_NEWS</option>
					<option value="celebrity">CELEBRITY</option>
					<option value="tv">TV</option>
					<option value="taste">TASTE</option>
					<option value="style">STYLE</option>
					<option value="religion">RELIGION</option>
					<option value="travel">TRAVEL</option>
					<option value="women">WOMEN</option>
					<option value="black_voices">BLACK_VOICES</option>
					<option value="impact">IMPACT</option>
					<option value="food">FOOD</option>
					<option value="latino_voices">LATINO_VOICES</option>
					<option value="code">CODE</option>
					<option value="parents">PARENTS</option>
					<option value="college">COLLEGE</option>
					<option value="teen">TEEN</option>
					<option value="divorce">DIVORCE</option>
					<option value="post50">POST50</option>
					<option value="gps_for_the_soul">GPS_FOR_THE_SOUL</option>
					<option value="good_news">GOOD_NEWS</option>
					<option value="weddings">WEDDINGS</option>
					<option value="gay_voices">GAY_VOICES</option>
				</select><br>
				<br>
				<input class = "btn btn-primary" onclick="populateArticleTrend();" type="button" value="submit">
			</form>
			<p>Hold down the Ctrl (windows) / Command (Mac) button to select
				multiple options</p>
			<br>
			<h1 id="result_id"></h1>
			<table id="articles_id" style="width: 100%">
			</table>
			<span id = "error_id"> </span>
		</div>
	</div>
	<!-- /.container -->
	<script
		src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js">
		
	</script>
	<script src="js/main.js"></script>
</body>
</html>
