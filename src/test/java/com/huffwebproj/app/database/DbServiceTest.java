package com.huffwebproj.app.database;

import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import com.huffwebproj.app.SearchResult;
import com.huffwebproj.app.parser.Article;
import com.huffwebproj.app.parser.Section;
import com.mongodb.DBCursor;

public class DbServiceTest {
	private Set<Article> mockArticleSet;
	private final String COLLECTION_NAME = Collection.ARTICLE.getCollectionName();
	String url1;
	String url2;
	String url3;
	String url4;
	String url5;
	String url6;
	String url7;
	String title1;
	String title2;
	String title3;
	String title4;
	String title5;
	String title6;
	String title7;
	String section1;
	String section2;
	String section3;
	String section4;
	String section5;
	String section6;
	String section7;

	@Before
	public void setUp() throws Exception {

		url1 = "http://www.huffingtonpost.com/2014/12/20/bringing-home-boyfriend-holidays_n_6360924.html?utm_hp_ref=mostpopular";
		title1 = "comments";
		section1 = Section.COMEDY.getSectionName();

		url2 = "http://www.huffingtonpost.com/2014/12/20/bringing-home-boyfriend-holidays_n_6360924.html?utm_hp_ref=mostpopular";
		title2 = "bringing boyfriend to home on holidays";
		section2 = Section.COMEDY.getSectionName();

		url3 = "http://www.huffingtonpost.com/";
		title3 = "bringing boyfriend to home on holidays";
		section3 = Section.COMEDY.getSectionName();

		url4 = "http://www.huffingtonpost.com/2014/12/20/bringing-home-boyfriend-holidays_n_6360924.html?utm_hp_ref=mostpopular";
		title4 = "bringing boyfriend to home on holidays";
		section4 = Section.BOOKS.getSectionName();

		url5 = "";
		title5 = "bringing boyfriend to home on holidays";
		section5 = Section.BUSINESS.getSectionName();

		url6 = null;
		title6 = "";
		section6 = Section.CODE.getSectionName();

		url7 = null;
		title7 = null;
		section7 = null;

		mockArticleSet = new HashSet<Article>();
		assertTrue(mockArticleSet.add(new Article(url1,title1,section1)));
		assertTrue(mockArticleSet.add(new Article(url2,title2,section2)));
		assertFalse(mockArticleSet.add(new Article(url3,title3,section3)));
		assertTrue(mockArticleSet.add(new Article(url4,title4,section4)));
		assertTrue(mockArticleSet.add(new Article(url5,title5,section5)));
		assertTrue(mockArticleSet.add(new Article(url6,title6,section6)));
		assertTrue(mockArticleSet.add(new Article(url7,title7,section7)));
		assertEquals(mockArticleSet.size(),6);
	}

	@After
	public void tearDown() throws Exception {
		mockArticleSet = null;
		//		DBConnectionManager.closeDbInstance();
	}

	@Test
	public void insertArticleCollectionTest() {
		DbService.dropCollection(COLLECTION_NAME);
		DbService.insert(COLLECTION_NAME,mockArticleSet);
	}

	@Test
	public void insertResultCollectionTest() {
		DbService.dropCollection(Collection.RESULT.getCollectionName());
		Set<Article> articleResultCollection = new HashSet<Article>();
		articleResultCollection.add(new Article(url1,title1,section1));
		articleResultCollection.add(new Article(url2,title2,section2));
		articleResultCollection.add(new Article(url3,title3,section3));
		articleResultCollection.add(new Article(url4,title4,section4));
		articleResultCollection.add(new Article(url5,title5,section5));
		SearchResult result = new SearchResult(123L,articleResultCollection);
		DbService.insert(Collection.RESULT.getCollectionName(),result);
		DbService.dropCollection(Collection.RESULT.getCollectionName());
	}

	@Test
	public void querySpecificCollectionTest() {
		DbService.dropCollection(COLLECTION_NAME);
		DbService.insert(COLLECTION_NAME,mockArticleSet);
		DBCursor dbCursor = DbService.querySpecificCollection(COLLECTION_NAME,Section.COMEDY.getSectionName());
		assertNotNull(dbCursor);
		assertEquals(dbCursor.count(),2);
		DbService.dropCollection(COLLECTION_NAME);
	}

	@Test
	public void queryAllCollectionTest() {
		DbService.dropCollection(COLLECTION_NAME);
		DbService.insert(COLLECTION_NAME,mockArticleSet);
		DBCursor dbCursor = DbService.queryAllCollection(COLLECTION_NAME);
		assertNotNull(dbCursor);
		assertEquals(dbCursor.count(),4);
		DbService.dropCollection(COLLECTION_NAME);
	}

	@Test
	public void dropCollectionTest() {
		DbService.dropCollection(COLLECTION_NAME);
	}

}
