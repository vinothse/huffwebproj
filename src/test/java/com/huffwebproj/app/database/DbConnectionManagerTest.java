package com.huffwebproj.app.database;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.mongodb.DB;

public class DbConnectionManagerTest {
	
	private final String DB_NAME = "huffwebprojdb";
	private DB db1;
	private DB db2;
	
	@Before
	public void setUp() throws Exception {
		db1 = DbConnectionManager.getDbInstance(DB_NAME);
		db2 = DbConnectionManager.getDbInstance(DB_NAME);
	}

	@After
	public void tearDown() throws Exception {
//		DBConnectionManager.closeDbInstance();
	}

	@Test
	public void test() {
		assertTrue(db1.equals(db2));
		assertEquals(db1.hashCode(),db2.hashCode());
	}

}
