package com.huffwebproj.app.twitter;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import twitter4j.Twitter;
import twitter4j.TwitterException;

public class TwitterServiceTest {

	private Twitter twitter;
	private int WOEID = com.huffwebproj.app.util.WOEID.NEW_YORK_CITY.getWoeid();
	private String trend1 = "#ThreeWordsSheWantsToHear";
	private String trend2 = "#HERESTO5SOSMEMORIES";
	private String trend3 = "#17Years2008OfNash64";
	private String trend4 = "psndown";
	private String trend5 = "Mariah Who";
	private String trend6 = "";
	private String trend7 = null;
	private String trend8 = "#ThreeWords@SheWants%123ToHear";
	private String trend9 = "#23456";
	private String trend10 = "$%!";
	private String trend11 = "#TheBachelor";
	
	@Before
	public void setUp() throws Exception {
		twitter = TwitterService.getTwitterInstance();
	}

	@After
	public void tearDown() throws Exception {
		twitter = null;
		trend1 = null;
		trend2 = null;
		trend3 = null;
		trend4 = null;
		trend5 = null;
		trend6 = null;
		trend7 = null;
		trend8 = null;
		trend9 = null;
		trend10 = null;
		trend11 = null;
	}

	@Test
	public void trendsServiceTest() {
		List<TwitterTrend> trendList = null;
		try {
			trendList = TwitterService.trendsService(twitter, WOEID);
		} catch (TwitterException e) {
			e.printStackTrace();
		}
		assertNotNull(trendList);
		assertEquals(trendList.size(),10);
		for(TwitterTrend trend : trendList){
			assertNotNull(trend);
			System.out.println("TREND_NAME : " + trend.getName());
			System.out.println("TREND_QUERY : " + trend.getQuery());
			System.out.println("TREND_URL : " + trend.getUrl());
			System.out.println("------------------------------------------");
		}
	}
	
	@Test
	public void processTrendTest(){
		String processedTrend1 = TwitterService.processTrend(trend1);
		assertNotNull(processedTrend1);
		assertEquals("Three Words She Wants Hear",processedTrend1);
		
		String processedTrend2 = TwitterService.processTrend(trend2);
		assertNotNull(processedTrend2);
		assertEquals("HERESTO5SOSMEMORIES",processedTrend2);
		
		String processedTrend3 = TwitterService.processTrend(trend3);
		assertNotNull(processedTrend3);
		assertEquals("Years Nash 17 2008 64",processedTrend3);
		
		String processedTrend4 = TwitterService.processTrend(trend4);
		assertNotNull(processedTrend4);
		assertEquals("psndown",processedTrend4);
		
		String processedTrend5 = TwitterService.processTrend(trend5);
		assertNotNull(processedTrend5);
		assertEquals("Mariah Who",processedTrend5);
		
		String processedTrend6 = TwitterService.processTrend(trend6);
		assertNotNull(processedTrend6);
		assertEquals("",processedTrend6);
		
		String processedTrend7 = TwitterService.processTrend(trend7);
		assertNull(processedTrend7);
		
		String processedTrend8 = TwitterService.processTrend(trend8);
		assertNotNull(processedTrend8);
		assertEquals("Three Words She Wants Hear 123",processedTrend8);
		
		String processedTrend9 = TwitterService.processTrend(trend9);
		assertNotNull(processedTrend9);
		assertEquals("23456",processedTrend9);
		
		String processedTrend10 = TwitterService.processTrend(trend10);
		assertNotNull(processedTrend10);
		assertEquals("$%!",processedTrend10);
		
		String processedTrend11 = TwitterService.processTrend(trend11);
		assertNotNull(processedTrend11);
		assertEquals("Bachelor",processedTrend11);
	}

	
}
