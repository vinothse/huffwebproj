package com.huffwebproj.app;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import twitter4j.TwitterException;

import com.huffwebproj.app.database.Collection;
import com.huffwebproj.app.database.DbService;
import com.huffwebproj.app.parser.Article;
import com.huffwebproj.app.parser.Section;
import com.huffwebproj.app.util.Mode;

public class RunnerTest {

	private String articleName1;
	private String articleName2;
	private String articleName3;
	private String articleName4;
	private String articleName5;
	private String articleName6;

	private String trend1;
	private String trend2;
	private String trend3;
	private String trend4;
	private String trend7;
	private String trend10;
	private String trend11;

	@Before
	public void setUp() throws Exception {
		trend1 = "Three Words She Wants Hear";
		trend2 = "2014";
		trend3 = "times 2014 new year";
		trend4 = "years happy celeb";
		trend7 = "bestmemoriesof 2014";
		trend10 = "Times Square";
		trend11 = "Happy New Years";

		articleName1 = "15 Inspiring happy Literary Quotes That Will Start Your New Year Off Right";
		articleName2 = "welcome new year 2014";
		articleName3 = "new year resolution no more";
		articleName4 = "1 of a kind";
		articleName5 = "The Most Insanely Delicious Recipes From Food Bloggers In 2014";
		articleName6 = "Times Square on high on New Year Celebrity";
	}

	@After
	public void tearDown() throws Exception {
		DbService.dropCollection(Collection.ARTICLE.getCollectionName());

		trend1 = null;
		trend2 = null;
		trend3 = null;
		trend4 = null;
		trend7 = null;
		trend10 = null;
		trend11 = null;

		articleName1 = null;
		articleName2 = null;
		articleName3 = null;
		articleName4 = null;
		articleName5 = null;
		articleName6 = null;
	}

	@Test
	public void runnerTest() {
		//Fetch Trending articles from HuffingtonPost (FRONT_PAGE SECTION) based on Twitter trending topics
		Map<Article, Integer> trendingArticleMap = null;
		try {
			trendingArticleMap = Runner.runner(Section.COMEDY, Mode.PARSE_AND_QUERY);
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertNotNull(trendingArticleMap);

		System.out.println("TRENDING HUFFINGTONPOST");
		Iterator<Entry<Article, Integer>> it = trendingArticleMap.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Article, Integer> pairs = (Map.Entry<Article, Integer>)it.next();
			System.out.println("TITLE: " + pairs.getKey().getTitle());
			System.out.println("SECTION: " + pairs.getKey().getSectionName());
			System.out.println("URL: " + pairs.getKey().getUrl());
		}
		System.out.println("RESULT SET SIZE : " + trendingArticleMap.size());
	}


	@Test
	/*
	 * trend1 = "Three Words She Wants Hear";
	 * trend7 = "bestmemoriesof 2014";
	 * trend10 = "Times Square";
		trend11 = "Happy New Years";

		trend2 = "HERESTO5SOSMEMORIES";
		trend3 = "times 2014 new year";
		trend4 = "years happy celeb";
		trend5 = "Mariah Who";
		trend6 = "Page 1 365";

		trend8 = "Three Words She Wants 123 Hear";
		trend9 = "23456";


		articleName1 = "15 Inspiring happy Literary Quotes That Will Start Your New Year Off Right";
		articleName2 = "welcome new year 2014";
		articleName3 = "new year resolution no more";
		articleName4 = "1 of a kind";
		articleName5 = "The Most Insanely Delicious Recipes From Food Bloggers In 2014";
		articleName6 = "Times Square on high on New Year Celebrity";
	 */
	public void isArticleTrendingTest(){

		//Positive test
		
		int rank12 = Runner.isArticleTrending(trend2,articleName2);
		assertEquals(rank12,35);
		
		int rank13 = Runner.isArticleTrending(trend3,articleName6);
		assertEquals(rank13,18);
		
		int rank11 = Runner.isArticleTrending(trend10,articleName6);
		assertEquals(rank11,15);
		
		int rank2 = Runner.isArticleTrending(trend7,articleName2);
		assertEquals(rank2,6);
		
		int rank3 = Runner.isArticleTrending(trend11,articleName3);
		assertEquals(rank3,6);
		
		int rank5 = Runner.isArticleTrending(trend7,articleName5);
		assertEquals(rank5,6);

		int rank14 = Runner.isArticleTrending(trend4,articleName6);
		assertEquals(rank14,1);
		
		//Negative test
		int rank1 = Runner.isArticleTrending(trend1,articleName1);
		assertEquals(rank1,0);

		int rank4 = Runner.isArticleTrending(trend10,articleName4);
		assertEquals(rank4,0);

		int rank6 = Runner.isArticleTrending(null, null);
		assertEquals(rank6,0);

		int rank7 = Runner.isArticleTrending(null, "");
		assertEquals(rank7,0);

		int rank8 = Runner.isArticleTrending("", null);
		assertEquals(rank8,0);

		int rank9 = Runner.isArticleTrending("", "");
		assertEquals(rank9,0);

		int rank10 = Runner.isArticleTrending(null, "XXX");
		assertEquals(rank10,0);
	}
}
