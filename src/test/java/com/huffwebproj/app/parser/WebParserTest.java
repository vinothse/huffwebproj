package com.huffwebproj.app.parser;

import java.util.Set;

import junit.framework.TestCase;


public class WebParserTest extends TestCase {
	
	final String POLITICS = "politics";

	public WebParserTest( String testName )
	{
		super( testName );
	}

	public void testUrlParser(){
		WebParser webParser = new WebParser();
		Set<Article> resultSet = webParser.urlParser(Section.EDUCATION);
		assertNotNull(resultSet);
		
		System.out.println("Printing contents of parsed URLs");
		for(Article itr : resultSet){
			System.out.println("TITLE: " + itr.getTitle());
			System.out.println("SECTION: " + itr.getSectionName());
			System.out.println("URL: " + itr.getUrl());
			System.out.println("----------------------------------------------------------------------------");
		}
		System.out.println("SIZE : " + resultSet.size());
	}

}
