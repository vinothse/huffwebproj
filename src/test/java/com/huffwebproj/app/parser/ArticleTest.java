package com.huffwebproj.app.parser;

import java.util.HashSet;
import java.util.Set;

import junit.framework.TestCase;
import static org.junit.Assert.*;

public class ArticleTest extends TestCase{

	Article article1;
	Article article2;
	Article article3;
	Article article4;
	Article article5;
	Article article6;
	Article article7;
	Article article8;
	Article article9;
	Article article10;
	Article article11;

	Set<Article> resultSet;

	public ArticleTest( String testName ){
		super( testName );
	}

	protected void setUp() {

		resultSet = new HashSet<Article>();

		String url1 = "http://www.huffingtonpost.com/2014/12/20/bringing-home-boyfriend-holidays_n_6360924.html?utm_hp_ref=mostpopular";
		String title1 = "comments";
		String section1 = Section.COMEDY.getSectionName();

		String url2 = "http://www.huffingtonpost.com/2014/12/20/bringing-home-boyfriend-holidays_n_6360924.html?utm_hp_ref=mostpopular";
		String title2 = "'Celebrity Apprentice' Fires Keshia Knight Pulliam For Not Speaking To Bill Cosby";
		String section2 = Section.COMEDY.getSectionName();

		String url3 = "http://www.huffingtonpost.com/";
		String title3 = "'Celebrity Apprentice' Fires Keshia Knight Pulliam For Not Speaking To Bill Cosby";
		String section3 = Section.COMEDY.getSectionName();

		String url4 = "http://www.huffingtonpost.com/2014/12/20/bringing-home-boyfriend-holidays_n_6360924.html?utm_hp_ref=mostpopular";
		String title4 = "bringing boyfriend to home on holidays";
		String section4 = Section.BOOKS.getSectionName();

		String url5 = "";
		String title5 = "bringing boyfriend to home on holidays";
		String section5 = Section.BUSINESS.getSectionName();

		String url6 = null;
		String title6 = "";
		String section6 = Section.CODE.getSectionName();

		String url7 = null;
		String title7 = null;
		String section7 = null;

		String url8 = null;
		String title8 = null;
		String section8 = null;

		String url9 = "";
		String title9 = null;
		String section9 = Section.BUSINESS.getSectionName();

		String title10 =  "Fox News' Megyn Kelly Defends Bill De Blasio";
		String section10 =  "POLITICS";
		String url10 = "http://www.huffingtonpost.com/2015/01/06/megyn-kelly-bill-de-blasio-defends-reappointment-of-judge_n_6422610.html?utm_hp_ref=politics&ir=Politics";

		String title11 = "Fox News' Megyn Kelly Defends Bill De Blasio";
		String section11 = "POLITICS";
		String url11 = "http://www.huffingtonpost.com/2015/01/06/megyn-kelly-bill-de-blasio-defends-reappointment-of-judge_n_6422610.html?ir=Media";

		article1 = new Article(url1, title1, section1);
		article2 = new Article(url2, title2, section2);
		article3 = new Article(url3, title3, section3);
		article4 = new Article(url4, title4, section4);
		article5 = new Article(url5, title5, section5);
		article6 = new Article(url6, title6, section6);
		article7 = new Article(url7, title7, section7);
		article8 = new Article(url8, title8, section8);
		article9 = new Article(url9, title9, section9);
		article10 = new Article(url10, title10, section10);
		article11 = new Article(url11, title11, section11);
	}

	protected void tearDown(){
		article1 = null;
		article2 = null;
		article3 = null;
		article4 = null;
		article5 = null;
		article6 = null;
		article7 = null;
		article8 = null;
		article9 = null;
		article10 = null;
		article11 = null;
	}

	public void testEquals(){

		//Positive test :
		//url diff but title/section same
		assertTrue(article2.equals(article3));
		assertTrue(article10.equals(article11));


		//Negative test : 
		//title not same although section is equal; hence objects are not same
		assertFalse(article1.equals(article3));
		//section diff; hence objects are not same
		assertFalse(article1.equals(article2));
		assertFalse(article1.equals(article4));
		assertFalse(article2.equals(article4));
		assertFalse(article3.equals(article4));
		assertFalse(article5.equals(article6));
		assertFalse(article5.equals(article7));
		assertFalse(article5.equals(article9));
		assertFalse(article6.equals(article7));
		//all null
		assertFalse(article7.equals(article8));

		//Test for adding equal objects to a Set collection
		assertEquals(resultSet.add(article2),true);
		assertEquals(resultSet.add(article3),false);
		assertEquals(resultSet.add(article10),true);
		assertEquals(resultSet.add(article11),false);

		for(Article a : resultSet){
			System.out.println("RESULT SET TITILE: "+a.getTitle());
		}
	}

	public void testHashCode(){

		//Contract : Equal objects must have equal hashcode
		//Positive Test
		assertEquals(article1.hashCode(),article1.hashCode());
		assertEquals(article2.hashCode(),article2.hashCode());
		assertEquals(article3.hashCode(),article3.hashCode());
		assertEquals(article4.hashCode(),article4.hashCode());
		assertEquals(article2.hashCode(),article3.hashCode());
		assertEquals(article7.hashCode(),article8.hashCode());
		assertEquals(article10.hashCode(),article11.hashCode());

		//Contract : Unequal objects must have unequal hashcode
		//Negative test
		assertNotEquals(article1.hashCode(),article3.hashCode());
		assertNotEquals(article1.hashCode(),article2.hashCode());
		assertNotEquals(article1.hashCode(),article4.hashCode());
		assertNotEquals(article2.hashCode(),article4.hashCode());
		assertNotEquals(article3.hashCode(),article4.hashCode());
		assertNotEquals(article5.hashCode(),article6.hashCode());
		assertNotEquals(article5.hashCode(),article7.hashCode());
		assertNotEquals(article6.hashCode(),article7.hashCode());
		assertNotEquals(article5.hashCode(),article9.hashCode());
	}
}

