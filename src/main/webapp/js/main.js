/**
 * AJAX call for populating the result without refreshing the whole page
 * @author - vinothse
 */
function populateArticleTrend() {
	/* alert("http://localhost:8080/huffwebproj/input?section="
				+ document.getElementById("select_id").value); */
//	e.preventDefault(); 
	$('#articles_id').html("");
	$("#error_id").html("");
	var sectionArray = $("#select_id").val();
	var urlTail = sectionArray.join(",");

	$.ajax({
		url : "http://localhost:8080/huffwebproj/input?section="
			+ urlTail,
			success : function(articles) {
				if(articles == "error"){
					$("#error_id").html("<b>Error: API Limit Exceeded, Please try after some time.</b>");
				}
				else if(articles == "No articles"){
					$("#error_id").html("<b>No Trending articles found under this section in HuffingtonPost. Please try a different section.</b>");
				}
				else{
					$('#result_id').html("Result");
					var $articles = $('#articles_id');
					$articles.append('<tr>' + '<th>' + 'Section' + '</th>'
							+ '<th>' + 'Title' + '</th>' + '<th>' + 'URL'
							+ '</th>' + '</tr>');
					$.each(articles, function(i, article) {
						$articles.append('<tr>' + '<td>' + article.section
								+ '</td>' + '<td>' + article.title + '</td>'
								+ '<td>' + '<a href=' + article.url + '>'
								+ article.url + '</a>' + '</td>' + '</tr>');
					});
				}
			}
	});
}


