package com.huffwebproj.app;

import java.util.Set;

import com.huffwebproj.app.parser.Article;

public class SearchResult {
	private long timestamp;
	private Set<Article> articleResultCollection;
	
	public SearchResult(long timestamp, Set<Article> articleResultCollection){
		this.timestamp = timestamp;
		this.articleResultCollection = articleResultCollection;
	}
	
	public long getTimestamp(){
		return timestamp;
	}
	
	public Set<Article> getArticleCollection(){
		return articleResultCollection;
	}
}
