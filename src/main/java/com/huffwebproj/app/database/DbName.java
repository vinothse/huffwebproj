package com.huffwebproj.app.database;

public enum DbName{
	HUFF_WEB_PROJ("huffwebprojdb");
	
	private final String dbName;

	private DbName(String dbName) {
		this.dbName = dbName;
	}
	
	public String getDbName() {
		return this.dbName;
	}
}
