package com.huffwebproj.app.database;

import java.net.UnknownHostException;
import com.mongodb.DB;
import com.mongodb.MongoClient;

public class DbConnectionManager {
	
	//caching objects for lazy initialization
	private static MongoClient mongoClient = null;
	private static DB db = null;

	private static final int PORT = 27017; 
	private static final String LOCALHOST = "localhost";

	public static DB getDbInstance(String dbName) throws UnknownHostException{
		//lazy initialization
		if(db == null){
			if(mongoClient == null){
				mongoClient = new MongoClient(LOCALHOST, PORT);
			}
			db = mongoClient.getDB(dbName);
		} 
		return db;
	}
	
	public static void closeDbInstance(){
		if(mongoClient != null){
			mongoClient.close();
		}
	}

}
