package com.huffwebproj.app.database;

import java.net.UnknownHostException;
import java.util.Set;
//import java.util.logging.Logger;
import com.huffwebproj.app.SearchResult;
import com.huffwebproj.app.parser.Article;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

public class DbService {

	private static final String DB_NAME = DbName.HUFF_WEB_PROJ.getDbName();
//	private static final Logger LOGGER = Logger.getLogger(DbService.class.getName());

	/**
	 * create and return DBCollection object using DbConnectionManager.getDbInstance method
	 * @author - vinothse
	 */
	private static DBCollection getDbCollection(String collectionName){
		DBCollection collection = null;
		try {
			DB db = DbConnectionManager.getDbInstance(DB_NAME);
			collection = db.getCollection(collectionName);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return collection;
	}


	/**
	 * create and return BasicDBObject from the article POJO
	 * @author - vinothse
	 */
	private static BasicDBObject toDBObject(Article article){
		if(article == null){
			return null;
		}
		BasicDBObject document = null;
		String title = article.getTitle();
		String url = article.getUrl();
		String sectionName = article.getSectionName();
		if(title != null && url != null && sectionName != null){
			document = new BasicDBObject();
			document.put("title", title);
			document.put("url", url);
			document.put("section", sectionName);
		}

		return document;
	}


	/**
	 * INSERT : Set<Article> into the collection of MONGODB
	 * @author - vinothse
	 */
	public static void insert(String collectionName, Set<Article> articleSet){
		if(articleSet == null || articleSet.isEmpty() || collectionName == null || collectionName.isEmpty()){
			return;
		}
		try {
			DBCollection collection = getDbCollection(collectionName);
			for (Article item : articleSet) {
				BasicDBObject document = toDBObject(item);
				//add document to the collection
				if(document != null){
					collection.insert(document);
				}
			}
		} finally {
			//DBConnectionManager.closeDbInstance();
		}
	}


	/**
	 * DROP : drop collection of MONGODB
	 * @author - vinothse
	 */
	public static void dropCollection(String collectionName){
		if(collectionName == null || collectionName.isEmpty()){
			return;
		}
		try {
			DBCollection collection = getDbCollection(collectionName);
			collection.drop();
		} finally {
			//			DBConnectionManager.closeDbInstance();
		}
	}


	/**
	 * QUERY : query specific attribute of the collection of MONGODB
	 * @author - vinothse
	 */
	public static DBCursor querySpecificCollection(String collectionName, String sectionName){
		if(collectionName == null || collectionName.isEmpty() || sectionName == null || sectionName.isEmpty()){
			return null;
		}
		try {
			DBCollection collection = getDbCollection(collectionName);
			DBCursor cursor = collection.find(new BasicDBObject("section", sectionName));
			return cursor;
		} finally {
			//			DBConnectionManager.closeDbInstance();
		}
	}


	/**
	 * QUERY : query all attributes of the collection of MONGODB
	 * @author - vinothse
	 */
	public static  DBCursor queryAllCollection(String collectionName){
		if(collectionName == null || collectionName.isEmpty()){
			return null;
		}
		try {
			DBCollection collection = getDbCollection(collectionName);
			DBCursor cursor = collection.find();
			return cursor;
		} finally {
			//			DBConnectionManager.closeDbInstance();
		}
	}



	/**
	 * INSERT : SearchResult into the collection of MONGODB
	 * @author - vinothse
	 */
	public static void insert(String collectionName, SearchResult result) {
		if(collectionName == null || collectionName.isEmpty() || result == null){
			return;
		}

		DBCollection collection = getDbCollection(collectionName);
		BasicDBObject document = toDBObject(result);
		
		if(document != null){
			collection.insert(document);
		}
	}

	/**
	 * create and return BasicDBObject from the SearchResult POJO
	 * @author - vinothse
	 */
	private static BasicDBObject toDBObject(SearchResult result) {
		if(result == null){
			return null;
		}
		BasicDBObject document = new BasicDBObject();
		long timestamp = result.getTimestamp();
		document.put("timestamp", timestamp);
		Set<Article> articleCollection = result.getArticleCollection();
		BasicDBList dbl = new BasicDBList();
		if(articleCollection != null){
			for(Article article : articleCollection){
				BasicDBObject articleDocument = new BasicDBObject();
				articleDocument.append("title",article.getTitle());
				articleDocument.append("section",article.getSectionName());
				articleDocument.append("url",article.getUrl());
				dbl.add(articleDocument);
			}		
			document.put("articles", dbl);
			return document;
		}
		document.put("articles", "");
		return document;
	}
}
