package com.huffwebproj.app.database;

public enum Collection{
	ARTICLE("articlecollection"),
	RESULT("resultcollection");
	
	private final String collectionName;

	private Collection(String collectionName) {
		this.collectionName = collectionName;
	}
	
	public String getCollectionName() {
		return this.collectionName;
	}
}
