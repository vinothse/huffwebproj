package com.huffwebproj.app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import com.huffwebproj.app.database.Collection;
import com.huffwebproj.app.database.DbService;
import com.huffwebproj.app.parser.Article;
import com.huffwebproj.app.parser.Section;
import com.huffwebproj.app.parser.WebParser;
import com.huffwebproj.app.twitter.TwitterService;
import com.huffwebproj.app.twitter.TwitterTrend;
import com.huffwebproj.app.util.Mode;
import com.huffwebproj.app.util.WOEID;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;


public class Runner {

	private static final Logger LOGGER = Logger.getLogger(Runner.class.getName());


	/**
	 * Main runner method of the project:
	 * - Retrieves Set<Article> that is trending based on the twitter trends, input section & mode (PARSE_AND_QUERY/QUERY_ONLY) 
	 * 
	 * @author - vinothse
	 * @throws TwitterException 
	 */
	public static Map<Article, Integer> runner(Section section, Mode mode) throws TwitterException{

		LOGGER.info(LOGGER.getName() + ": Section: " + section.getSectionName());
		Map<Article, Integer> trendArticleMap = new HashMap<Article, Integer>();

		//Twitter : find trending topics based on New York city (currently hard-coded)
		Twitter twitter = TwitterService.getTwitterInstance();
		List<TwitterTrend> trendList =  TwitterService.trendsService(twitter, WOEID.NEW_YORK_CITY.getWoeid());
		//Print Twitter trends
		printTwitterTrends(trendList);

		//Parse the URLs from the HuffingtonPost, persist in MongoDB
		if(mode == Mode.PARSE_AND_QUERY){
			WebParser webParser = new WebParser();
			Set<Article> resultSet = webParser.urlParser(section);
			if(resultSet != null){
				LOGGER.info(LOGGER.getName() + ": PARSING DONE: SIZE: " + resultSet.size());
			}

			//Insert parsed articles into article collection of huffwebprojdb in MongoDB
			DbService.dropCollection(Collection.ARTICLE.getCollectionName());
			DbService.insert(Collection.ARTICLE.getCollectionName(),resultSet);
		}

		//Make query all on article collection and traverse to find the matching articles
//		DBCursor cursor = DbService.queryAllCollection(Collection.ARTICLE.getCollectionName());
		
		//Query MongoDB for the specific section Name and traverse to find the matching articles
		DBCursor cursor = DbService.querySpecificCollection(Collection.ARTICLE.getCollectionName(),section.getSectionName());
		while(cursor.hasNext()){
			DBObject dbObj = cursor.next();
			if(dbObj != null){
				if(trendList != null){					
					for(TwitterTrend trend : trendList){
						String trendTokens = trend.getName();
						int trendRank = isArticleTrending(trendTokens, (String) dbObj.get("title"));
						if( trendRank != 0 ){
							//trendingArticleSet.add(new Article((String) dbObj.get("url"),(String) dbObj.get("title"),(String) dbObj.get("section")));
							Article article = new Article((String) dbObj.get("url"),(String) dbObj.get("title"),(String) dbObj.get("section"));
							LOGGER.info(LOGGER.getName() + ": TRENDING ARTICLE ADDED TO THE MAP : TITLE: " + article.getTitle() + ", RANK: " + trendRank);
							//System.out.println("TEST" + article.getTitle());
							if(trendArticleMap.containsKey(article)){
								int oldRank = trendArticleMap.get(article);
								trendArticleMap.put(article, oldRank + trendRank);
								//								System.out.println("TEST: CONTAINS : OLD RANK" + oldRank);
							}
							else{
								trendArticleMap.put(article, trendRank);
							}
						}
					}
				}
			}
		}
		return trendArticleMap;
	}


	/**
	 * Sort Map according to value i.e. Rank(Integer)
	 * @author - vinothse
	 */
	public static Map<Article, Integer> sortTrendArticleMap(Map<Article, Integer> trendArticleMap){

		if(trendArticleMap == null || trendArticleMap.isEmpty()){
			return trendArticleMap;
		}
//		LOGGER.info(LOGGER.getName() + ": sortTrendArticleMap: input size: " + trendArticleMap.size());
		//System.out.println("TREND MAP WITHOUT SORT : " + trendArticleMap.size());
		Set<Map.Entry<Article, Integer>> set = trendArticleMap.entrySet();
		List<Map.Entry<Article, Integer>> list = new ArrayList<Map.Entry<Article, Integer>>(set); 
		Collections.sort( list, new Comparator<Map.Entry<Article, Integer>>() 
				{ public int compare( Map.Entry<Article, Integer> o1, Map.Entry<Article, Integer> o2 ) 
				{ return (o2.getValue()).compareTo( o1.getValue() ); } } );

		Map<Article, Integer> rankedTrendArticleMap = new LinkedHashMap<Article, Integer>();

		for (Map.Entry<Article, Integer> entry : list) {
			rankedTrendArticleMap.put(entry.getKey(), entry.getValue());
//			LOGGER.info(LOGGER.getName() + ": rankedTrendArticleMap: TITLE: " + entry.getKey().getTitle() + " RANK: " + entry.getValue());
		}

		return rankedTrendArticleMap;
	}


	/**
	 * Find whether an article is Trending or not! - based on twitter trend tokens
	 * Populate rank for each article based on full trend match, full token/partial token matches - Used for ranking the results
	 * @author - vinothse
	 */
	public static int isArticleTrending(String trend, String articleName) {
		if(trend == null || trend.isEmpty() || articleName == null || articleName.isEmpty()){
			return 0;
		}
//		LOGGER.info(LOGGER.getName() + "isArticleTrending()" + ": trend: "+ trend + ", article: " + articleName);
		String[] tokens = trend.split(" ");
		int rank = 0;
		String[] articleTokens = articleName.split(" ");
		
		//MATCH condition1 : check if full/partial trend is contained in the article name
		if(trend.length() > 1 && articleName.toLowerCase().contains(trend.toLowerCase())){
			if(articleTokens != null && articleTokens.length > 0){
				for(String s : articleTokens){
					if(trend.length() > 1 && s.equalsIgnoreCase(trend)){
//						LOGGER.info(LOGGER.getName() + "FULL TREND EXACT MATCH vs ARTICLE NAME TOKENS: RANK 20" + ", trend: "+ trend + ", article: " + articleName);
						rank += 20;
					}
				}	
			}
//			LOGGER.info(LOGGER.getName() + "FULL TREND PARTIAL MATCH vs ARTICLE NAME: RANK 10" + ", trend: "+ trend + ", article: " + articleName);
			rank += 15;
		}
		//MATCH condition2 : check if full/partial trend tokens are contained in the article name
		else if(tokens != null && tokens.length > 0){
			for(String token : tokens){
				if(token.length() > 1 && articleName.toLowerCase().contains(token.toLowerCase())){
					if(articleTokens != null && articleTokens.length > 0){
						for(String s : articleTokens){
							if(token.length() > 1 && s.equalsIgnoreCase(token)){
//								LOGGER.info(LOGGER.getName() + "SINGLE TOKEN EXACT MATCH vs ARTICLE NAME TOKENS: RANK 5" + ", token: "+ token + ", article: " + articleName);
								rank += 5;
							}
						}
					}
//					LOGGER.info(LOGGER.getName() + "SINGLE TOKEN PARTIAL MATCH vs ARTICLE NAME: RANK++" + ", token: "+ token + ", article: " + articleName);
					rank++;
				}
			}
		}
//		LOGGER.info(LOGGER.getName() + "FINAL RANK: " + rank);
		return rank;
	}


	/**
	 * Print TwitterTrends on console
	 * @author - vinothse
	 */
	public static void printTwitterTrends(List<TwitterTrend> trendList){
		if(trendList != null  && !trendList.isEmpty()){
			//System.out.println("PRINTING TWITTER TRENDS : " + trendList.size());
			LOGGER.info(LOGGER.getName() + ": PRINTING TWITTER TRENDS : " + trendList.size());
			for(TwitterTrend trend : trendList){
				//System.out.println("NAME : " + trend.getName());
				//System.out.println("--------------------");
				LOGGER.info(LOGGER.getName() + ": TREND_NAME : " + trend.getName());
			}
		}
	}
}


class ValueComparator implements Comparator<Article> {

	Map<Article, Integer> base;
	public ValueComparator(Map<Article, Integer> base) {
		this.base = base;
	}

	@Override
	public int compare(Article o1, Article o2) {
		if (base.get(o1) >= base.get(o2)) {
			return -1;
		} else {
			return 1;
		}
	}
}
