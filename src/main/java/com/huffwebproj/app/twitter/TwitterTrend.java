package com.huffwebproj.app.twitter;

public class TwitterTrend {
	private String name;
	private String url;
	private String query;
	
	public TwitterTrend(String name, String url, String query){
		this.name = name;
		this.url = url;
		this.query = query;
	}
	
	public String getName(){
		return this.name;
	}
	public String getUrl(){
		return this.url;
	}
	public String getQuery(){
		return this.query;
	}
}
