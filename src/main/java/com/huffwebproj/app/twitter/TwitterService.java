package com.huffwebproj.app.twitter;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import com.huffwebproj.app.util.*;
import twitter4j.Trend;
import twitter4j.Trends;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterService {

	//	private static final Logger LOGGER = Logger.getLogger(Runner.class.getName());

	private static final String CONSUMER_KEY = Util.getOAuthKeyPropFile(OAuth.CONSUMER_KEY);
	private static final String CONSUMER_SECRET = Util.getOAuthKeyPropFile(OAuth.CONSUMER_SECRET);
	private static final String ACCESS_TOKEN = Util.getOAuthKeyPropFile(OAuth.ACCESS_TOKEN);
	private static final String ACCESS_TOKEN_SECRET = Util.getOAuthKeyPropFile(OAuth.ACCESS_TOKEN_SECRET);


	/**
	 * Creates and output Twitter instance after o-auth steps
	 * @author - vinothse
	 */
	public static Twitter getTwitterInstance(){
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true)
		.setOAuthConsumerKey(CONSUMER_KEY)
		.setOAuthConsumerSecret(CONSUMER_SECRET)
		.setOAuthAccessToken(ACCESS_TOKEN)
		.setOAuthAccessTokenSecret(ACCESS_TOKEN_SECRET);
		TwitterFactory twitterFactory = new TwitterFactory(cb.build());
		return  twitterFactory.getInstance();
	}

	
	/**
	 * Retrieves List<TwitterTrends> i.e. trending twitter words through TwitterAPI
	 * @author - vinothse
	 * @throws TwitterException 
	 */
	public static List<TwitterTrend> trendsService(Twitter twitter, int WOEID) throws TwitterException{

		//		try {
		Trends trends = twitter.trends().getPlaceTrends(WOEID);
		if(trends != null){
			List<TwitterTrend> trendList = new ArrayList<TwitterTrend>();
			Trend[] trendArray = trends.getTrends();
			for(int i=0;i<trendArray.length;i++){

				String processedName = processTrend(trendArray[i].getName());
				if(processedName != null){
					trendList.add(new TwitterTrend(processedName, trendArray[i].getQuery(), trendArray[i].getURL()));
				}
			}
			return trendList;
		}
		//		} catch (TwitterException e) {
		//			LOGGER.info(LOGGER.getName() + ": TwitterException: " + e.getErrorMessage() + "XXXX");
		//			e.printStackTrace();
		//		} catch(Exception e){
		//			e.printStackTrace();
		//		}
		return null;
	}


	/**
	 * Returns the processed Twitter trend topic as a space separated words
	 * @author - vinothse
	 */
	public static String processTrend(String name){
		if(name == null || name.isEmpty() || StringUtils.isAllUpperCase(name)){
			return name;
		}
		//		System.out.println("INPUT: " + name);
		//		System.out.println("isAllUpper? : " + StringUtils.isAllUpperCase(name));

		if(name.contains("#")){
			//Strip special characters & hash
			String processedName = name.replace("#", "").replaceAll("[^a-zA-Z0-9]","");
			//			System.out.println("AFTER REM # & special char: " + processedName);

			//Strip numbers and check if all char are UPPER_CASE -> Return original string
			String decimalStrip = processedName.replaceAll("[0-9]","");
			//			System.out.println("DECIMAL STRIP : "+decimalStrip);
			if(StringUtils.isAllUpperCase(decimalStrip)){
				//				System.out.println("All CAPS and number present : returning the original string");
				return processedName;
			}

			//Rip the numbers and words and form a space separated tokens
			String ALPHA_PATTERN = "(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])";
			String NUM_PATTERN = "[a-zA-Z]+";
			StringBuilder alphaBuilder = new StringBuilder();
			StringBuilder numBuilder = new StringBuilder();
			String[] numArray = processedName.split(NUM_PATTERN);
			if(numArray != null){
				for (String split : numArray) {
					//					System.out.println("NUMBER : " + split);
					if(!split.equals(" ")){
						numBuilder.append(split.trim());
						numBuilder.append(" ");
					}
				}

			}
			String[] splitArray = decimalStrip.split(ALPHA_PATTERN);
			if(splitArray != null){
				for (String split : splitArray) {
					//					System.out.println(split);
					if(!split.equals(" ") && !checkStopWord(split)){
						alphaBuilder.append(split.trim());
						alphaBuilder.append(" ");
					}
				}

				String a = alphaBuilder.toString().trim();
				String b = numBuilder.toString().trim();

				//				System.out.println("ALPHA_BUILDER : " + a);
				//				System.out.println("NUM_BUILDER : " + b);

				return a.concat(" " +b).trim();
			}
		}
		return name;
	}


	/**
	 * Removes stop word from twitter token
	 * @author - vinothse
	 */
	public static boolean checkStopWord(String token){
		if(token == null || token.isEmpty()){
			return false;
		}
		token = token.toLowerCase().trim();
		if(token.equals("the") || 
				token.equals("a") || 
				token.equals("of") || 
				token.equals("on") || 
				token.equals("at") ||
				token.equals("that") ||
				token.equals("their") ||
				token.equals("in") ||
				token.equals("and") ||
				token.equals("are") ||
				token.equals("but") ||
				token.equals("be") ||
				token.equals("by") ||
				token.equals("into") ||
				token.equals("if") ||
				token.equals("is") ||
				token.equals("it") ||
				token.equals("for")||
				token.equals("no") ||
				token.equals("not")||
				token.equals("or") ||
				token.equals("such") ||
				token.equals("then") ||
				token.equals("there") ||
				token.equals("these") ||
				token.equals("they") ||
				token.equals("this") ||
				token.equals("to") ||
				token.equals("was") ||
				token.equals("will") ||
				token.equals("with") ||
				token.equals("were") ||
				token.equals("up") ||
				token.equals("down")
				){
			return true;
		}
		return false;
	}
}
