package com.huffwebproj.app.parser;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashSet;
import java.util.ListIterator;
import java.util.Set;
//import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class WebParser {

//	private static final Logger LOGGER = Logger.getLogger(WebParser.class.getName());
	
	
	/**
	 * Parse the article urls from a specific section from HuffingtonPost using jsoup API
	 * @author - vinothse
	 */
	public Set<Article> urlParser(Section section){

		final String URL_BASE = "http://huffingtonpost.com/";
		Set<Article> resultSet = new HashSet<Article>();

		try {
			StringBuilder sb = new StringBuilder(URL_BASE);
			sb.append(section.getUrlString());			
			Document doc = Jsoup.connect(sb.toString()).get();
			ListIterator<Element> listItr = doc.select("a").listIterator();
			while(listItr.hasNext()){
				Element link = listItr.next();
				String absHref = link.attr("abs:href");
//				if(absHref != null && absHref.contains("huffingtonpost") && absHref.contains(".html") && !absHref.contains("#comments") 
//						&& Pattern.compile(Pattern.quote(section.getSectionName()), Pattern.CASE_INSENSITIVE).matcher(absHref).find()){ 
				if(absHref != null && absHref.contains("huffingtonpost") && absHref.contains(".html") && !absHref.contains("#comments")){ 
					String text = link.text();
//					String textExtra = link.attr("span");
//					LOGGER.info(LOGGER.getName() + ": textExtra ->" + textExtra);
					if(text != null && !text.isEmpty()){
//						LOGGER.info(LOGGER.getName() + "ARTICLE PARSED: " + text);
						Article article = new Article(absHref,text.trim(),section.getSectionName());
						if(!resultSet.contains(article)){
							resultSet.add(article);
						}
						
					}
				}
			}
			return resultSet;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return null;
	}


}
