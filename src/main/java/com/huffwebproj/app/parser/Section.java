package com.huffwebproj.app.parser;

public enum Section {

	FRONT_PAGE("","FRONT_PAGE"), 
	POLITICS("politics","POLITICS"),
	BUSINESS("business","BUSINESS"), 
	ENTERTAINMENT("entertainment","ENTERTAINMENT"), 
	MEDIA("media","MEDIA"), 
	WORLDPOST("theworldpost","WORLDPOST"), 
	SPORTS("sports","SPORTS"), 
	TECH("tech","TECH"), 
	GREEN("green","GREEN"), 
	COMEDY("comedy","COMEDY"),
	HEALTHY_LIVING("healthy-living","HEALTHY_LIVING"), 
	ARTS("arts","ARTS"), 
	SCIENCE("science","SCIENCE"), 
	SMALL_BIZ("small-business","SMALL_BIZ"), 
	CRIME("crime","CRIME"), 
	WEIRD_NEWS("weird-news","WEIRD_NEWS"), 
	CELEBRITY("celebrity","CELEBRITY"), 
	TV("tv","TV"), 
	TASTE("taste","TASTE"), 
	STYLE("style","STYLE"), 
	BOOKS("books","BOOKS"), 
	RELIGION("religion","RELIGION"), 
	TRAVEL("travel","TRAVEL"),
	WOMEN("women","WOMEN"), 
	BLACK_VOICES("black-voices","BLACK_VOICES"), 
	IMPACT("impact","IMPACT"), 
	FOOD("food","FOOD"),
	LATINO_VOICES("latino-voices","LATINO_VOICES"), 
	CODE("huffpost-code","CODE"), 
	EDUCATION("education","EDUCATION"), 
	PARENTS("parents","PARENTS"),
	COLLEGE("college","COLLEGE"), 
	TEEN("teen","TEEN"), 
	DIVORCE("divorce","DIVORCE"), 
	POST50("50","POST50"), 
	GPS_FOR_THE_SOUL("gps-for-the-soul","GPS_FOR_THE_SOUL"),
	GOOD_NEWS("good-news","GOOD_NEWS"), 
	WEDDINGS("weddings","WEDDINGS"),
	HOME("home","HOME"),
	GAY_VOICES("gay-voices","GAY_VOICES");

	private final String urlString;
	private final String sectionName;

	private Section(String urlString, String sectionName) {
		this.urlString = urlString;
		this.sectionName = sectionName;
	}

	public String getUrlString() {
		return this.urlString;
	}
	
	public String getSectionName() {
		return this.sectionName;
	}
}
