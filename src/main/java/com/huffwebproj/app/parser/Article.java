package com.huffwebproj.app.parser;

//import java.util.logging.Logger;

public class Article {

	private String url;
	private String title;
	private String section;
	//private static final Logger LOGGER = Logger.getLogger(Article.class.getName());

	public Article(String url, String title, String section){
		this.url = url;
		this.title = title;
		this.section = section;
	}

	//Getter methods
	public String getUrl(){
		return url;
	}
	public String getTitle(){
		return title;
	}
	public String getSectionName(){
		return section;
	}

	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 * Returns true if Title & Section equal
	 * @author vinothse
	 */
	public boolean equals(Object obj) {
		;
		//null instanceof Object will always return false
		if (!(obj instanceof Article)){
//			LOGGER.info(LOGGER.getName() + ": Obj not instance of Article: return false");
			return false;
		}
		if (obj == this){
			return true;
		}
		if((Article) obj == null || this == null || this.title == null || ((Article) obj).title == null){
			return false;
		}
		return this.title.equals(((Article) obj).title) && this.section.equals(((Article) obj).section);
	}


	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 * Hashcode is computed based on the hashcode of section and title objects
	 * @author vinothse
	 */
	public int hashCode() {
		int result = 17;
		int c1 = 0;
		if(section != null){
			c1 = section.hashCode();
		}
		int c2 = 0;
		if(title != null){
			c2 = title.hashCode();
		}
		result = 31 * result + c1;
		result = 31 * result + c2;
		//LOGGER.info(LOGGER.getName() + ": HASHCODE METHOD CALLED ON: " + this.title + ", hashcode: "+result);
		return result;
	}
}
