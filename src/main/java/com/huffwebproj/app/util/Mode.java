package com.huffwebproj.app.util;

public enum Mode {
	PARSE_AND_QUERY,
	QUERY_ONLY
}
