package com.huffwebproj.app.util;

public enum WOEID {
	
	UNITED_STATES(23424977),
	WORLD(1),
	NEW_YORK_CITY(2459115),
	NEW_YORK_STATE(2347591);


	private final int woeid;

	private WOEID(int woeid) {
		this.woeid = woeid;
	}

	public int getWoeid() {
		return this.woeid;
	}

}
