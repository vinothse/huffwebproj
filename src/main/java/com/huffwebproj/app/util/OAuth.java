package com.huffwebproj.app.util;

public enum OAuth {
	CONSUMER_KEY,
	CONSUMER_SECRET,
	ACCESS_TOKEN,
	ACCESS_TOKEN_SECRET
}
