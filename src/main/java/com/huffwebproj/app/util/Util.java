package com.huffwebproj.app.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Util {
	
	/**
	 * Get appropriate contents from property file based on the oAuth given for Twitter API authentication
	 * @author - vinothse
	 */
	public static String getOAuthKeyPropFile(OAuth oAuth) {
		Properties prop = new Properties();
		String propFileName = "config.properties";
		try{
			InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(propFileName);
			prop.load(inputStream);
		}
		catch(IOException e){
			e.printStackTrace();
		}
		switch (oAuth) {
		case CONSUMER_KEY:
			return prop.getProperty("CONSUMER_KEY");
		case CONSUMER_SECRET:
			return prop.getProperty("CONSUMER_SECRET");
		case ACCESS_TOKEN:
			return prop.getProperty("ACCESS_TOKEN");
		case ACCESS_TOKEN_SECRET:
			return prop.getProperty("ACCESS_TOKEN_SECRET");
		default:
			return null;
		}
	}
}
