package com.huffwebproj.app.servlet;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import twitter4j.TwitterException;

import com.google.gson.Gson;
import com.huffwebproj.app.Runner;
import com.huffwebproj.app.SearchResult;
import com.huffwebproj.app.database.Collection;
import com.huffwebproj.app.database.DbService;
import com.huffwebproj.app.parser.Article;
import com.huffwebproj.app.parser.Section;
import com.huffwebproj.app.util.Mode;


@WebServlet("/input")
public class GetInputServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(GetInputServlet.class.getName());


	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GetInputServlet() {
		super();
	}


	/**
	 * Prepare response object by running the runner against the sections retrieved from request object
	 * @author - vinothse
	 */
	public void doGet(HttpServletRequest request,HttpServletResponse response)
			throws ServletException, IOException
	{
		StringBuilder responseBuilder = new StringBuilder();
		String[] inputArray = request.getParameter("section").split(",");
		Map<Article, Integer>  trendingArticleMap = null;
		Map<Article, Integer>  allSectionTrendingArticleMap = new HashMap<Article, Integer>();
		Gson gson = new Gson();
		boolean errorFlag = false;
		//		int count = 0;
		//		boolean trendArticleNotFoundFlag = false;
		if(inputArray != null){
			LOGGER.info(LOGGER.getName() + ": SectionArray: " + inputArray.length);
			for(String s : inputArray){
				Section section = getSection(s);
				LOGGER.info(LOGGER.getName() + ": Call Runner: " + section.getSectionName());
				try {
					trendingArticleMap = Runner.runner(section, Mode.PARSE_AND_QUERY);
				} catch (TwitterException e) {
					//Twitter Exception API Limit exceeeded error
					errorFlag = true;
					responseBuilder = new StringBuilder();
					responseBuilder.append("error");
				}
				if(trendingArticleMap != null && !trendingArticleMap.isEmpty()){
					//					trendArticleNotFoundFlag = false;
					allSectionTrendingArticleMap.putAll(trendingArticleMap);
				}
				//				else{
				////					trendArticleNotFoundFlag = true;
				//					//Append error when no request was successful
				//					if(allSectionTrendingArticleMap.isEmpty()){
				//						LOGGER.severe(LOGGER.getName() + " : Runner returned null or empty: Append error to response ");
				//						responseBuilder.append("error");
				//						break;
				//					}
				//				}
			}
			if(!allSectionTrendingArticleMap.isEmpty()){
				Set<Article> finalTrendingArticleSet = null;
				//sort map according to value i.e. Rank and retrieve the keyset of the map
				Map<Article, Integer> finalTrendingArticleMap = Runner.sortTrendArticleMap(allSectionTrendingArticleMap);
				if(finalTrendingArticleMap != null){
					finalTrendingArticleSet = finalTrendingArticleMap.keySet();
				}
				//append the set to the gson object
				if(finalTrendingArticleSet != null && !finalTrendingArticleSet.isEmpty()){
					responseBuilder.append(gson.toJson(finalTrendingArticleSet));
					response.setContentType("application/json");
					
					//store the result articleSet in the database with timestamp
					SearchResult result = new SearchResult(System.currentTimeMillis(),finalTrendingArticleSet);
					DbService.insert(Collection.RESULT.getCollectionName(),result);
				}
			}
			else{
				if(!errorFlag){
					LOGGER.severe(LOGGER.getName() + " : No Trending articles found ");
					responseBuilder.append("No articles");
				}
			}
			PrintWriter out = null;
			try {
				out = response.getWriter();
				out.write(responseBuilder.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			finally{
				if(out != null){
					out.flush();
					out.close();
				}	
			}
		}
	}


	/**
	 * Get Section object from the input section string
	 * @author - vinothse
	 */
	private Section getSection(String s) {

		if(s == null || s.isEmpty()){
			return null;
		}
		switch(s){

		case "front_page":
			return Section.FRONT_PAGE;

		case "home":
			return Section.HOME;

		case "entertainment":
			return Section.ENTERTAINMENT;

		case "politics":
			return Section.POLITICS;

		case "business":
			return Section.BUSINESS;

		case "sports":
			return Section.SPORTS;

		case "worldpost":
			return Section.WORLDPOST;

		case "education":
			return Section.EDUCATION;

		case "tech":
			return Section.TECH;

		case "books":
			return Section.BOOKS;

		case "media":
			return Section.MEDIA;

		case "green":
			return Section.GREEN;

		case "comedy":
			return Section.COMEDY;

		case "healthy_living":
			return Section.HEALTHY_LIVING;

		case "arts":
			return Section.ARTS;

		case "science":
			return Section.SCIENCE;

		case "small_biz":
			return Section.SMALL_BIZ;

		case "crime":
			return Section.CRIME;

		case "weird_news":
			return Section.WEIRD_NEWS;

		case "celebrity":
			return Section.CELEBRITY;

		case "tv":
			return Section.TV;

		case "taste":
			return Section.TASTE;

		case "style":
			return Section.STYLE;

		case "religion":
			return Section.RELIGION;

		case "travel":
			return Section.TRAVEL;

		case "women":
			return Section.WOMEN;

		case "black_voices":
			return Section.BLACK_VOICES;

		case "impact":
			return Section.IMPACT;

		case "food":
			return Section.FOOD;

		case "latino_voices":
			return Section.LATINO_VOICES;

		case "code":
			return Section.CODE;

		case "parents":
			return Section.PARENTS;

		case "college":
			return Section.COLLEGE;

		case "teen":
			return Section.TEEN;

		case "divorce":
			return Section.DIVORCE;

		case "post50":
			return Section.POST50;

		case "gps_for_the_soul":
			return Section.GPS_FOR_THE_SOUL;

		case "good_news":
			return Section.GOOD_NEWS;

		case "weddings":
			return Section.WEDDINGS;

		case "gay_voices":
			return Section.GAY_VOICES;

		default:
			return null;
		}
	}
}